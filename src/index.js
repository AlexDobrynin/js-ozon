import cart from "./modules/cart";
import search from "./modules/searchWithFilters";
import {searchStore} from "./searchSore";
import catalogFilter from "./modules/catalogFilter";


// Инициализировать действия на корзине
cart();
// инициализируем поиск
search();
// инициализировать
catalogFilter();
// Заполнить товары на странице при загрузке страницы
searchStore.reset().doSearch();
