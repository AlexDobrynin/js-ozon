import getData from "./modules/getData";
import renderGoods from "./modules/renderGoods";

export const searchStore = {
    filter: {
        title: undefined,
        priceMin: undefined,
        priceMax: undefined,
        category: undefined,
        isSale: undefined,
    },
    reset: () => {
        Object.keys(key => searchStore.filter[key] = undefined);

        return searchStore;
    },
    doSearch: () => {
        getData(searchStore.filter).then(data => renderGoods(data));
    },
};
