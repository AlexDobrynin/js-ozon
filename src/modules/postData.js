export default () => {
    return fetch(`${process.env.API_URL}/goods`, {
        method: "POST", body: JSON.stringify({
            title: "Игровая приставка Sony PlayStation 2",
            price: 17400,
            sale: false,
            img: "https://cdn1.ozone.ru/multimedia/c400/1033180284.jpg",
            hoverImg: "https://cdn1.ozone.ru/multimedia/c400/1033180283.jpg",
            category: "Игровая приставка"
        }), headers: {
            "Content-type": "application/json; charset=UTF-8",
        },
    })
        .then(response => {
            if (response.ok) {
                return response.json();
            }

            throw Error(response.statusText || `Error with http status code: ${response.status}`);
        });
};
