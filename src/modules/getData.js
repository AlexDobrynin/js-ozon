export default ({title, priceMin, priceMax, isSale, category} = {}) => {
    let searchParam = {};

    if (title) {
        searchParam = Object.assign({"title_like": title}, searchParam);
    }

    if (priceMin) {
        searchParam = Object.assign({"price_gte": priceMin}, searchParam);
    }

    if (priceMax) {
        searchParam = Object.assign({"price_lte": priceMax}, searchParam);
    }

    if (isSale === true) {
        searchParam = Object.assign({"sale": true}, searchParam);
    }

    if (category) {
        searchParam = Object.assign({"category": category}, searchParam);
    }

    let queryString = "";

    if (Object.keys(searchParam).length) {
        queryString = "?" + Object.entries(searchParam).map(pair => pair.map(encodeURIComponent).join("=")).join("&");
    }

    return fetch(`${process.env.API_URL}/goods${queryString}`)
        .then(response => {
            if (response.ok) {
                return response.json();
            }

            throw Error(response.statusText || `Error with http status code: ${response.status}`);
        })
        .then(json => json);
};
