import goodTemplate from "../templates/goodTemplate";

export default (goods) => {
    const goodsWrapper = document.querySelector(".goods");

    if (goodsWrapper) {
        goodsWrapper.innerHTML = "";

        if (goods.length) {
            goods.forEach((item) => {
                goodsWrapper.insertAdjacentHTML("beforeend", goodTemplate(item));
            });
        } else {
            goodsWrapper.insertAdjacentHTML("beforeend", "<h1>Ничего не найдено.</h1>");
        }

    }
};

