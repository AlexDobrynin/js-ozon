import {searchStore} from "../searchSore";

export default () => {
    const inputSearchDelay = 500;
    const searchInput = document.querySelector("input.search-wrapper_input[type='text']");

    if (searchInput) {
        let inputTimer = null;
        searchInput.addEventListener("input", (eventInput) => {
            searchStore.filter.title = eventInput.target.value;
            clearTimeout(inputTimer);
            inputTimer = setTimeout(() => searchStore.doSearch(), inputSearchDelay);
        });
    }

    const setPriceFilter = (element) => {
        let val = parseFloat(element.value) || undefined;

        const {priceMin, priceMax} = searchStore.filter;

        if (element.id === "min") {
            if (val > priceMax) {
                val = undefined;
                element.value = "";
            }

            searchStore.filter.priceMin = val;
        }

        if (element.id === "max") {
            if (val < priceMin) {
                val = undefined;
                element.value = "";
            }

            searchStore.filter.priceMax = val;
        }

        searchStore.doSearch();
    };

    document.querySelectorAll(".filter-price input").forEach((element) => {
        let inputTimer = null;
        element.addEventListener("input", (eventInput) => {
            clearTimeout(inputTimer);
            inputTimer = setTimeout(setPriceFilter, inputSearchDelay, eventInput.target);
        });
    });

    const isSale = document.querySelector(".filter-check_label");

    if (isSale) {
        isSale.addEventListener("click", (eventClick) => {
            eventClick.preventDefault();
            searchStore.filter.isSale = !searchStore.filter.isSale;
            const checkMark = isSale.querySelector(".filter-check_checkmark") || {};

            if (checkMark) {
                if (searchStore.filter.isSale) {
                    checkMark.classList.add("checked");
                } else {
                    checkMark.classList.remove("checked");
                }
            }

            searchStore.doSearch();
        });
    }
};
