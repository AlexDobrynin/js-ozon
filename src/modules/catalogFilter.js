import {searchStore} from "../searchSore";

export default () => {
    const catalogBlock = document.querySelector(".catalog-button");
    const catalogMenu  = document.querySelector(".catalog-button .catalog");

    if (catalogBlock && catalogMenu) {
        catalogBlock.addEventListener("click", (eventClick) => {
            eventClick.preventDefault();
            if (catalogMenu.style.display !== "block") {
                catalogMenu.style.display = "block";
            } else {
                catalogMenu.style.display = "none";
            }

            if (eventClick.target.tagName === "LI") {
                const category = eventClick.target.innerText;

                if (typeof category === "string" && category.length) {
                    searchStore.filter.category = category;
                    searchStore.doSearch();
                }
            }
        });
    }
};
