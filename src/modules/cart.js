export default () => {
    const cart = document.getElementById("cart");

    const modalCart = document.querySelector("div.cart");
    /** @param {DOMTokenList} classList */
    const canClose = (classList) => classList.contains("cart-close") || classList.contains("cart");

    if (cart && modalCart) {
        cart.addEventListener("click", () => {
            modalCart.style.display = "flex";
        });

        modalCart.addEventListener("click", (eventClick) => {
            const currentClass = eventClick.target.classList;

            if (currentClass && canClose(currentClass)) {
                modalCart.style.display = "none";
            }
        });
    }
};
