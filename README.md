# Учебный проект по Ozon-Js
В проекте используется 
[json-server](https://github.com/typicode/json-server). Для запуска сервера необходимо его установить и запустить:
```bash
json-server --watch db/db.json
```

Настройки API endpoint хранятся в `.env` файле в переменной `API_URL`
