const Dotenv = require("dotenv-webpack");

module.exports = {
    watchOptions: {
        ignored: ['**/node_modules/', '**/db/db.json/'],
    },
    plugins: [
        new Dotenv()
    ],
};
